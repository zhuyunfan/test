package com.aaa.controller;

import com.aaa.anno.AccessLimit;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class TestController {

    @AccessLimit
    @RequestMapping("/test")
    public String test(String requestKey, HttpServletRequest request){
        return "success";
    }

}
